// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyALFOYn0o9BLtkpfzxELP03ofW7j_PSLkM",
    authDomain: "exama-1ad5e.firebaseapp.com",
    databaseURL: "https://exama-1ad5e.firebaseio.com",
    projectId: "exama-1ad5e",
    storageBucket: "exama-1ad5e.appspot.com",
    messagingSenderId: "307760216399",
    appId: "1:307760216399:web:86a6297855d5ceeb3fed3a"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
