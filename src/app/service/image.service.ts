import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ImageService {

  public images:string[] = [];
  public path:string = 'https://firebasestorage.googleapis.com/v0/b/exama-1ad5e.appspot.com/o/'; 
 
constructor() {
  this.images[0] = this.path +'biz.jpg' + '?alt=media';
  this.images[1] = this.path +'entermnt.jpg' + '?alt=media';
  this.images[2] = this.path +'politics-icon.png' + '?alt=media';
  this.images[3] = this.path +'sport.jpg' + '?alt=media';
  this.images[4] = this.path +'tech.jpg' + '?alt=media';
}

}
