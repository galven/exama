import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavComponent } from './nav/nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import {RouterModule, Routes } from '@angular/router';
import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';
import {MatCardModule} from '@angular/material/card';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material';
import {MatInputModule} from '@angular/material';
import {FormsModule}   from '@angular/forms';
import {MatExpansionModule} from '@angular/material/expansion';
import {HttpClientModule } from '@angular/common/http';
import {AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFirestoreModule, AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireAuth } from '@angular/fire/auth';
import { LoginComponent } from './login/login.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { DocformComponent } from './docform/docform.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { BlogPostComponent } from './blog-post/blog-post.component';
import { HttpClient } from '@angular/common/http';
import { SavedPostsComponent } from './saved-posts/saved-posts.component';



const appRoutes: Routes = [
{ path: 'sign-up', component: SignUpComponent},
{ path: 'login', component: LoginComponent},
{ path: 'welcome', component: WelcomeComponent},
{ path: 'blog-post/:id', component: BlogPostComponent},
{ path: 'saved-posts', component: SavedPostsComponent},
{ path: '', redirectTo: '/welcome', pathMatch: 'full'},
]

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    LoginComponent,
    SignUpComponent,
    DocformComponent,
    WelcomeComponent,
    BlogPostComponent,
    SavedPostsComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatCardModule,
    MatExpansionModule,
    AngularFireStorageModule,
    AngularFirestoreModule,
    MatSelectModule,
    HttpClientModule,
    AngularFireAuthModule,
    MatInputModule,
    FormsModule,
    MatFormFieldModule,
    AngularFireModule.initializeApp(environment.firebaseConfig, 'examA'),
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true}),
  ],
  providers: [AngularFirestore,AngularFireAuth],
  bootstrap: [AppComponent]
})
export class AppModule { }
